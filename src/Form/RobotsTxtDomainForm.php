<?php

namespace Drupal\domain_robots_txt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Defines a Robots.txt form for domains.
 */
class RobotsTxtDomainForm extends ConfigFormBase {

  /**
   * Domain ID of config.
   *
   * @var string
   */
  protected $domainId;

  /**
   * Constructs Drupal\domain_robots_txt\Form\RobotsTxtDomainForm
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteMatchInterface $routeMatch) {
    $this->setConfigFactory($config_factory);
    $this->domainId = $routeMatch->getParameter('domain_id');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_robots_txt_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::getConfigNameByDomainId($this->domainId)];
  }

  /**
   * Get config name by Domain ID.
   *
   * @param string $domain_id
   *   Domain ID.
   *
   * @return string
   *   Config name.
   */
  public static function getConfigNameByDomainId($domain_id) {
    return 'domain.config.' . $domain_id . '.robots_txt.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $domain_id = NULL) {
    // TODO:: check that domain id is correct and domain is exist.
    $this->domainId = $domain_id;
    $form['domain_id'] = [
      '#type' => 'value',
      '#value' => $domain_id,
    ];
    // TODO:
    $robots_txt = $this->config(self::getConfigNameByDomainId($domain_id))
      ->get('robots_txt');
    $form['robots_txt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Robots.txt file for "@type" domain', ['@type' => $domain_id]),
      // TODO: Is it only php7+? Yes?
      '#default_value' => $robots_txt ?? '',
      '#cols' => 60,
      '#rows' => 20,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns the value of a given storage setting.
   *
   * @param string $setting_name
   *   The setting name.
   * @param string|null $domain_id
   *   Domain ID. Use current domain ID if NULL.
   * @param mixed $default_value
   *   Default value for empty config value.
   *
   * @return mixed
   *   The setting value.
   */
  protected function getSetting($setting_name, $domain_id, $default_value = NULL) {
    $config = $this->config($this->getConfigNameByDomainId($domain_id));
    return $config->get($setting_name) ? $config->get($setting_name) : $default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $domain_id = $form_state->getValue('domain_id');
    $config = $this->config($this->getConfigNameByDomainId($domain_id));
    $values = $form_state->getValues();
    $config->set('robots_txt', $values['robots_txt']);
    $config->save();
    $message = $this->t('"Robots.txt file for "@type" domain was updated.', ['@type' => $domain_id]);
    // TODO: DI for drupal_set_message.
    drupal_set_message($message);
    // TODO: How about redirect on domains dashboard?
  }

}
